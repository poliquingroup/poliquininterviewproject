﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoliquinInterviewProject.Data
{
    public static class RESTfulAreaTranslator
    {
        /// <summary>
        /// Takes a string in the format "\d+SKM" and returns a long integer value representing the area. A value of -1 is returned for any value that cannot be successfully parsed.
        /// </summary>
        /// <param name="areaString"></param>
        /// <returns></returns>
        public static long Translate(string areaString)
        {
            // TODO: return the default for now
            long result = default(long);
            if (!long.TryParse(areaString, out result))
            {
                result = -1;
            }
            return result;
        }
    }
}