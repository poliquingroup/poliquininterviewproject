﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoliquinInterviewProject.Data
{
    /// <summary>
    /// http://json2csharp.com/
    /// </summary>
    public class RestResponse
    {
        public List<string> messages { get; set; }
        public List<Result> result { get; set; }
    }
}