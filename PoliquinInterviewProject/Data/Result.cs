﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoliquinInterviewProject.Data
{
    /// <summary>
    /// http://json2csharp.com/
    /// </summary>
    public class Result
    {
        public string country { get; set; }
        public string name { get; set; }
        public string abbr { get; set; }
        public string area { get; set; }
        public string largest_city { get; set; }
        public string capital { get; set; }
    }
}