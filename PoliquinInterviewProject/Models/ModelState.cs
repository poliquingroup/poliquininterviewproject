﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoliquinInterviewProject.Models
{
    /// <summary>
    /// Model for holding data related to a state entity.
    /// </summary>
    public class ModelState
    {
        public string Name { get; set; }
        public string LargestCity { get; set; }
        public long Area { get; set; }
    }
}