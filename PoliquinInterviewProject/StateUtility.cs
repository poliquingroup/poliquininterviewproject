﻿using PoliquinInterviewProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoliquinInterviewProject
{
    public static class StateUtility
    {
        /// <summary>
        /// Gets the state from the specified list that is most similarly sized (in area) to the specified state.
        /// </summary>
        /// <param name="state"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static ModelState GetStateWithMostSimilarArea(ModelState state, IEnumerable<ModelState> list)
        {
            // TODO: return some dummy data for now
            var result = new ModelState
            {
                Name = "[Unknown]",
                LargestCity = "[Unknown]",
                Area = -1,
            };            
            return result;
        }
    }
}