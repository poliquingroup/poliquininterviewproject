# Welcome to The Poliquin Group! #

This exercise should not take you more than one hour to complete. The results of this project will be used in discussions during future interviews.

### To complete this project: ###

* Ensure all Unit Tests are passing. Two are currently failing. The code you write to get these tests passing will be used in the next exercise(s).
* Modify the application so that the state with the most similar area is also displayed on the State Statistics page.
* Additional text should say something like "The state most closely related in size is California with 423,968 sq/km."
* Modify the application so the state (with the most similar area) displayed in #2 above is clickable. When clicked, the user should be redirected to the State Statistics page for that particular state.

### What we're looking for: ###

* Elegance - is your solution elegant?
* Readability - is your code easily readible... by a junior developer for instance?
* Documentation - did you clearly document your code so that your intentions are understood by future developers?
* Adaptability - did you write code/documentation that matches the style and architecture of the project?